// ===================THEORY==============================
/*
1. Як можна сторити функцію та як ми можемо її викликати?
Функцію можна створити трьиа способами:
-Function declaration (function nameFunction(parameters){});
-Function expression (const variable = function(){})
-Arrow function (const variable = () => {} or not)

Викликати функцію можна вказавши її ім'я і в круглих дужках вказавши відповідні аргументи або просто дужки:
nameFunction();

2. Що таке оператор return в JavaScript? Як його використовувати в функціях?
return - це оператор що завершує виконання функції та вказує значення, яке повертається викликачеві.
Він має необхідну участь у функції оскільки без нього функція невидимо повертає undefined. Також він повертає вказане значення функції як результат. Ставити його необхідно на кінець виконання функції, оскільки після нього код функції зупиняє своє виконання.

3. Що таке параметри та аргументи в функіях та в яких випадках вони використовуються?
Параметри функці ї- це оголошені в дужках функції місцедержателі, які приймають при виклику функції аргументи як значення і застосовують їх у роботі функції.

Аргументи функції - це фактичні значення, що подаються до функції при її виклику і ставляться на місце параметрів.

Використовуються тоді коли нам потрібно виконувати маніпуляції з їх участю.

4. Як передати функцію аргументом в іншу функцію?

Call back функція - це функція яка приймає участь як аргумент в іншій функції. Її участь в іншій функції виконується при передаванні її як аргумента.

function nameFunction(callBackFunction){}
nameFunction(callBackFunction);

*/

// ===================PRACTICE============================

// 1.
let numOne = parseInt(prompt('Введіть будьласка перше число!'));
let numTwo = parseInt(prompt('Введіть будьласка друге число!'));

while(isNaN(numOne)){
    numOne = parseInt(prompt('Введіть будьласка перше число!'));
}
while(isNaN(numTwo)){
    numTwo = parseInt(prompt('Введіть будьласка перше число!'));
}

function divideNumber(firstNum, secondNum){
    return firstNum / secondNum;
}

console.log(divideNumber(numOne, numTwo));
alert(divideNumber(numOne, numTwo));

// 2.

let operation = prompt('Введіть будьласка математичну операцію!');

let sum;
let minus;
let divide;
let mult;

switch(operation){
    case "+":
        sum = "+";
    break;
    case "-":
        minus = "-";
    break;
    case "/":
        divide = "/";
    break;
    case "*":
        mult = "*";
    break;
    default:
        alert("Операція введена не коректно!!!");
}

function operand(a, b, result){
    if(sum){
        result = a + b;    
    }else if(minus){
        result = a - b;
    }else if(divide){
        result = a / b;  
    }else if(mult){
        result = a * b;  
    }else{
        result = "Спробу ще раз!!!"
    }
    return result;
}

function mathOperation (num1, num2, fn){
    return fn(num1, num2);
}
alert(mathOperation(numOne, numTwo, operand));

// 3.

let factorial = parseInt(prompt('Введіть будьласка число!'));

while(isNaN(factorial)){
    factorial = parseInt(prompt('Введіть будьласка коректно число!'));
}

const factorialNumber = function(num){
    return num ? num * factorialNumber(num - 1) : 1;
}

console.log(factorialNumber(factorial));
alert(factorialNumber(factorial));